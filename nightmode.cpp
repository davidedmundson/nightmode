/********************************************************************

Copyright (C) 2010 David Edmundson <kde@davidedmundson.co.uk>

based on work by
Copyright (C) 2008, 2009 Martin Gräßlin <kde@martin-graesslin.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "nightmode.h"

#include <KAction>
#include <KActionCollection>
#include <KDebug>
#include <KConfig>
#include <KConfigGroup>
#include <KLocalizedString>
//#include <kwinglutils.h>


using namespace KWin;

KWIN_EFFECT( nightmode, NightmodeEffect )

NightmodeEffect::NightmodeEffect()
        : m_active( false )
        , activateAnimation( false )
        , deactivateAnimation( false )
{
    reconfigure( ReconfigureAll );

    KActionCollection* actionCollection = new KActionCollection( this );

    /*Added to kwins action collection by the config class*/
    KAction* a = (KAction*)actionCollection->addAction( "Nightmode" );
    a->setText( i18n( "Toggle Nightmode Effect" ));
    a->setGlobalShortcut( KShortcut( Qt::CTRL + Qt::META + Qt::Key_N ));
    connect(a, SIGNAL( triggered(bool) ), this, SLOT( toggle()));
}

NightmodeEffect::~NightmodeEffect()
{
}

void NightmodeEffect::reconfigure( ReconfigureFlags )
{
    /*    timeline.setDuration( animationTime( 250 ));*/
    KConfigGroup conf = EffectsHandler::effectConfig("Nightmode");
    m_brightness = conf.readEntry("brightness",50);
    effects->addRepaintFull();
}

void NightmodeEffect::prePaintScreen(ScreenPrePaintData &data, int time)
{
    effects->prePaintScreen(data, time);
}

void NightmodeEffect::postPaintScreen()
{
    effects->postPaintScreen();
    return;}

void NightmodeEffect::paintWindow(EffectWindow *w, int mask, QRegion region, WindowPaintData &data )
{
    if (m_active) {
        data.brightness *= (m_brightness / 100.0);
//         data.saturation *= (m_brightness / 100.0);
    }
    effects->paintWindow( w, mask, region, data );
}

void NightmodeEffect::toggle()
{
    m_active = !m_active;
    effects->addRepaintFull();
}

#include "nightmode.moc"
