/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

 Copyright (C) 2008, 2009 Martin Gräßlin <kde@martin-graesslin.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef KWIN_NIGHTMODE_H
#define KWIN_NIGHTMODE_H

#include <kwineffects.h>

class NightmodeEffect : public KWin::Effect
{
    Q_OBJECT
public:
    NightmodeEffect();
    ~NightmodeEffect();

    virtual void prePaintScreen(KWin::ScreenPrePaintData &data, int time);
    virtual void reconfigure( ReconfigureFlags );
    virtual void postPaintScreen();
    virtual void paintWindow(KWin::EffectWindow* w, int mask, QRegion region, KWin::WindowPaintData &data);

private slots:
    //turns the effect on or off (see m_active)
    virtual void toggle();

private:
    //whether the effect is enabled or not
    bool m_active;
    //darkness factor 0 = entirely black(clearly pointless)
    //100 = full brightness
    //don't set to more than 100
    //read from config on load
    unsigned int m_brightness;


    //FUTURE
    bool activateAnimation;
    bool deactivateAnimation;
};

#endif
