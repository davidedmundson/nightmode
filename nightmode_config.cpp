/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2007 Rivo Laks <rivolaks@hot.ee>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "nightmode_config.h"

#include <kwineffects.h>

#include <KLocale>
#include <KDebug>
#include <KActionCollection>
#include <KAction>
#include <KShortcutsEditor>
#include <KConfigGroup>


#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>

using namespace KWin;

// KWIN_EFFECT_CONFIG_FACTORY

KWIN_EFFECT_CONFIG( nightmode, NightmodeConfig )


NightmodeConfig::NightmodeConfig(QWidget* parent, const QVariantList& args) :
        KCModule(EffectFactory::componentData(), parent, args)
{
    QVBoxLayout* layout = new QVBoxLayout(this);

    // Shortcut config. The shortcut belongs to the component "kwin"!
    KActionCollection *actionCollection = new KActionCollection( this, KComponentData("kwin") );

    KAction* a = static_cast<KAction*>(actionCollection->addAction( "Nightmode" ));
    a->setText( i18n("Toggle Nightmode Effect" ));
    a->setProperty("isConfigurationAction", true);
    a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::META + Qt::Key_N));

    m_shortcutEditor = new KShortcutsEditor(actionCollection, this,
                                            KShortcutsEditor::GlobalAction, KShortcutsEditor::LetterShortcutsDisallowed);
    connect(m_shortcutEditor, SIGNAL(keyChange()), this, SLOT(changed()));
    layout->addWidget(m_shortcutEditor);

    QHBoxLayout* horizLayout = new QHBoxLayout(this);
    
    QLabel* label = new QLabel("Brightness",this);
    horizLayout->addWidget(label);
    
    m_brightnessSlider = new QSlider(Qt::Horizontal,this);
    m_brightnessSlider->setRange(20,80); //the plugin takes a brighness 0-100, but the two extremes are just silly to permit.
    connect(m_brightnessSlider,SIGNAL(valueChanged(int)),this,SLOT(changed()));
    horizLayout->addWidget(m_brightnessSlider);
    layout->addLayout(horizLayout);


    layout->addStretch();

    load();
}

NightmodeConfig::~NightmodeConfig()
{
    // Undo (only) unsaved changes to global key shortcuts
    m_shortcutEditor->undoChanges();
}

void NightmodeConfig::load()
{
    KCModule::load();
    KConfigGroup conf = EffectsHandler::effectConfig("Nightmode");

    unsigned int brightness = conf.readEntry("brightness",50);
    m_brightnessSlider->setValue(brightness);

    emit changed(false);
}

void NightmodeConfig::save()
{
    KCModule::save();
    KConfigGroup conf = EffectsHandler::effectConfig("Nightmode");

    m_shortcutEditor->save();    // undo() will restore to this state from now on

    unsigned int brightness = m_brightnessSlider->value();
    conf.writeEntry("brightness",brightness);

    emit changed(false);
    EffectsHandler::sendReloadMessage( "nightmode" );
}

void NightmodeConfig::defaults()
{
    m_shortcutEditor->allDefault();
    m_brightnessSlider->setValue(50);
    emit changed(true);
}


#include "nightmode_config.moc"
